Package: smoof
Type: Package
Title: Single and Multi-Objective Optimization Test Functions
Description: Provides generators for a high number of both single- and multi-
    objective test functions which are frequently used for the benchmarking of
    (numerical) optimization algorithms. Moreover, it offers a set of convenient
    functions to generate, plot and work with objective functions.
Version: 1.6.0.2
Date: 2020-02-17
Authors@R: c(person("Jakob", "Bossek", email = "j.bossek@gmail.com", role =
    c("aut", "cre")), person("Pascal", "Kerschke", email = "kerschke@uni-muenster.de",
    role = "ctb"))
Maintainer: Jakob Bossek <j.bossek@gmail.com>
URL: https://github.com/jakobbossek/smoof
BugReports: https://github.com/jakobbossek/smoof/issues
License: BSD_2_clause + file LICENSE
Depends: ParamHelpers (>= 1.8), checkmate (>= 1.1)
Imports: BBmisc (>= 1.6), ggplot2 (>= 2.2.1), RColorBrewer, plot3D,
        plotly, mco, Rcpp (>= 0.11.0), RJSONIO
Suggests: testthat, rPython (>= 0.0-5)
LazyData: yes
ByteCompile: yes
LinkingTo: Rcpp, RcppArmadillo
RoxygenNote: 6.1.1
NeedsCompilation: yes
Packaged: 2020-02-17 04:12:11 UTC; bossek
Author: Jakob Bossek [aut, cre],
  Pascal Kerschke [ctb]
Repository: CRAN
Date/Publication: 2020-02-18 09:00:02 UTC
Built: R 4.2.1; x86_64-w64-mingw32; 2022-07-24 04:37:52 UTC; windows
ExperimentalWindowsRuntime: ucrt
Archs: x64
